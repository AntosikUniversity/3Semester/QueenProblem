#ifndef __QUEEN__
#define __QUEEN__

#include <iostream>

class queen {
	private:
	int _x;
	int _y;
	int _color;
	public:
	/* init */
	queen(int x = -1, int y = -1, int color = 0) : _x(x), _y(y), _color(color) {};

	/* get & set*/
	int get_x();
	int get_y();
	int get_color();

	void set_x(int);
	void set_y(int);
	void set_pos(int, int);
	void set_color(int);

	/* manipulation */
	void draw();
	friend std::ostream & operator << (std::ostream & stream, queen & q);

	/* destructor */
	~queen();
};

#endif // __QUEEN__
