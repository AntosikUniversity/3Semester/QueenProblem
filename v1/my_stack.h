#ifndef __MY_STACK__
#define __MY_STACK__

#include <iostream>
using namespace std;

template<typename stack_type>
class my_stack {
	private:
	stack_type * _el;					// ������� �������
	my_stack * _nextEl;				// ��������� �������

	public:
	/* init */
	my_stack(stack_type elem = 0, my_stack * next = nullptr) {
		_el = new stack_type(elem);
		_nextEl = next;
	};


	/* manipulate */
	bool push(stack_type & elem) {
		if (_el == nullptr) {
			try {
				_nextEl = nullptr;
				_el = new stack_type(elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}

		} else {
			try {
				_nextEl = new my_stack(*_el, _nextEl);
				_el = new stack_type(elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}
		}
		return true;
	};

	bool pushP(stack_type * elem) {
		if (_el == nullptr) {
			try {
				_nextEl = nullptr;
				_el = new stack_type(*elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}

		} else {
			try {
				_nextEl = new my_stack(*_el, _nextEl);
				_el = new stack_type(*elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}
		}
		return true;
	};

	stack_type pop() {
		stack_type frac = *_el;
		my_stack * next = _nextEl;
		if (next) {
			_el = next->_el;
			_nextEl = next->_nextEl;
		} else {
			_el = nullptr;
			_nextEl = nullptr;
		}
		return frac;
	};

	void clear() {
		if (_el) delete _el;
		if (_nextEl) delete _nextEl;
		_el = nullptr;
		_nextEl = nullptr;
	}

	int empty() {
		return _nextEl ? false : true;
	}

	int length() {
		int res = 0;
		my_stack * temp = this;
		while (temp->_nextEl) { res++; temp = temp->_nextEl; }
		return res + 1;
	}

	void print() {
		my_stack * temp = this;
		while (temp->_nextEl) { cout << *(temp->_el) << " , "; temp = temp->_nextEl; }
		return;
	};

	friend ostream & operator << (ostream & stream, my_stack & stack) {
		my_stack * temp = &stack;
		while (temp->_nextEl) { stream << *(temp->_el) << " - "; temp = temp->_nextEl; }
		stream << "NULL";
		return stream;
	}

	/* get & set */
	stack_type * get_el() const {
		return _el;
	};

	stack_type * get_pos(int pos) {
		my_stack * temp = this;
		for (int i = 0; i < pos; i++) { temp = temp->_nextEl; }
		return temp->_el;
	};

	void set_el(stack_type _el) {
		_el = &frac;
		return;
	}

	my_stack * get_next_el() {
		return _nextEl;
	}


	/* desctructor */
	~my_stack() {
		if (_el) delete _el;
		if (_nextEl) delete _nextEl;
	};
};

#endif // __MY_STACK__
