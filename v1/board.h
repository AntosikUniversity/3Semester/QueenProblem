#ifndef __MY_STACK__
#include "my_stack.h"
#endif // __MY_STACK__

#ifndef __QUEEN__
#include "queen.h"
#endif // __QUEEN__

#ifndef __BOARD__
#define __BOARD__

#include <iostream>
#include <Windows.h>

const int SZ = 8;

class board {
	private:
	bool cells[SZ][SZ];
	public:

	/* init */
	board();
	void reinit();

	/* get & set */
	bool get_pos(int x, int y);
	void set_pos(int x, int y, bool flag);
	bool check_pos(int x, int y);

	/* manipulation */
	void draw_empty();
	void draw_queen(int x, int y);
	void redraw();

	/* destructor */
	~board();
};


#endif // __BOARD__
