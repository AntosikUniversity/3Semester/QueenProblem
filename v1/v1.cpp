#include "stdafx.h"
#include "queen.h"
#include "my_stack.h"
#include "board.h"
#include <iostream>
#include <fstream>
#include <locale.h>
#include <conio.h>

using namespace std;

fstream file("log.log");
board brd;
my_stack<queen> stack;
int lines = 0;
COORD coord;
DWORD hasWritten;
HANDLE hout = GetStdHandle(STD_OUTPUT_HANDLE);

struct pos {
	int x;
	int y;
} my_pos;

void setQueen(int, int);
void show_board();
void draw_stack();

int main() {
	setlocale(LC_ALL, "RU");
	char c;
	int x = 0, y = 0;
	file.clear();

	my_pos.x = 0;
	my_pos.y = 0;
	show_board();
	/*
	cout << "������� �������, �������� B5" << endl;
	cin >> c >> y;
	my_pos.x = c - 65;
	my_pos.y = y - 1;
	
	setQueen(my_pos.x, my_pos.y);*/
	file.close();
	system("pause");
	return 0;
}

void setQueen(int a, int y) {
	if (a == 8) a = 0;

	if (a == my_pos.x && lines > 1) {
		if (stack.get_pos(stack.length() - 2)->get_y() != my_pos.y) {
			cout << "ENDED";
			system("pause");
			exit(1);
		};
		file << "----------------------------------------------------------------------" << endl;
		file << "FOUND IT! Check it out: \n" << stack << endl;
		file << "----------------------------------------------------------------------\n\n" << endl;
		return;
	}

	stack.pushP(new queen());
	if (lines == 0) {
		(stack.get_el())->set_pos(a, y);
		brd.set_pos(a, y, true);
	}
	lines++;

	for (int i = 0; i < SZ; i++) {
		(stack.get_el())->set_pos(a, i);
		if (brd.check_pos(a, i)) {
			brd.set_pos(a, i, true);

			file << stack << endl;
			draw_stack();
			Sleep(250);

			setQueen(a + 1, i);
			brd.set_pos(a, i, false);
		}
	}

	stack.pop();
	lines--;
	return;
}

void show_board() {
	int k;
	coord.X = 0; coord.Y = 0;
	WriteConsole(hout, L" ", 1, &hasWritten, NULL);
	brd.draw_empty();
	brd.draw_queen(my_pos.x, my_pos.y);
	do {
		k = getch();
		if (k == 224) //���� ���������
		{
			k = getch();
			switch (k) {
				case 72: // up
					if (my_pos.y != 7) my_pos.y += 1;
					else my_pos.y = 0;
					break;
				case 80: // down
					if (my_pos.y != 0) my_pos.y -= 1;
					else my_pos.y = 7;
					break;
				case 75: // left
					if (my_pos.x != 0) my_pos.x -= 1;
					else my_pos.x = 7;
					break;
				case 77: // right
					if (my_pos.x != 7) my_pos.x += 1;
					else my_pos.x = 0;
					break;
			}
			show_board();
		} else if (k == 27) { // Esc
			system("CLS");
			system("pause");
			exit(0);
		} else if (k == 13) { // Enter
			setQueen(my_pos.x, my_pos.y);
			system("pause");
			exit(0);
		}
	} while (k != 13 || k != 27);
	system("pause");
	exit(0);
};

void draw_stack() {
	brd.draw_empty();
	my_stack<queen> * temp = &stack;
	while (temp->get_next_el()) {
		brd.draw_queen(temp->get_el()->get_x(), temp->get_el()->get_y());
		temp = temp->get_next_el();
	};
}