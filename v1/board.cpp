﻿#include "stdafx.h"
#define CONSOLE_SELECTED FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_BLUE
#define CONSOLE_DEFAULT FOREGROUND_GREEN | FOREGROUND_INTENSITY | BACKGROUND_BLUE
#define CONSOLE_BG BACKGROUND_BLUE

#ifndef __BOARD__
#include "board.h"
#endif // __BOARD__


/* init */
board::board() {
	reinit();
}

void board::reinit() {
	for (size_t i = 0; i < SZ; ++i) {
		for (size_t j = 0; j < SZ; ++j) {
			cells[i][j] = false;
		}
	}
}


/* get & set */
bool board::get_pos(int x, int y) {
	return cells[x][y];
}

void board::set_pos(int x, int y, bool flag) {
	cells[x][y] = flag;
}

bool board::check_pos(int x, int y) {
	for (int i = 0; i < SZ; i++) {
		if ((x != i) && get_pos(i, y)) {
			return false;
		}
	}

	for (int i = 0; i < SZ; i++) {
		if ((y != i) && get_pos(x, i)) {
			return false;
		}
	}

	for (int i = 1; i < SZ; i++) {
		if ((x - i) >= 0 && (y - i) >= 0 && get_pos(x - i, y - i)) {
			return false;
		}
		if ((x - i) >= 0 && (y + i) < 8 && get_pos(x - i, y + i)) {
			return false;
		}
		if ((x + i) < 8 && (y - i) >= 0 && get_pos(x + i, y - i)) {
			return false;
		}
		if ((x + i) < 8 && (y + i) < 8 && get_pos(x + i, y + i)) {
			return false;
		}
	}
	return true;
}


/* manipulation */
void board::draw_empty() {
	COORD coord;
	HANDLE hout = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD hasWritten;

	SetConsoleTextAttribute(hout, CONSOLE_DEFAULT);
	coord.X = 0; coord.Y = 0; SetConsoleCursorPosition(hout, coord);

	WriteConsole(hout, L"╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"│   │   │   │   │   │   │   │   │                                              ", 80, &hasWritten, NULL);
	WriteConsole(hout, L"╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝                                              ", 80, &hasWritten, NULL);

}

void board::draw_queen(int x, int y) {
	COORD coord;
	HANDLE hout = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD hasWritten;

	coord.X = 0; coord.Y = 0;
	SetConsoleCursorPosition(hout, coord);


	coord.Y = (SZ - y) * 2 - 1;
	coord.X = (x + 1) * 4 - 3;
	for (int i = 0; i < 3; i++, coord.X++)
		FillConsoleOutputAttribute(hout, CONSOLE_BG, 1, coord, &hasWritten);
	SetConsoleTextAttribute(hout, CONSOLE_SELECTED);
	coord.X -= 2;
	SetConsoleCursorPosition(hout, coord);
	WriteConsole(hout, L"X", 1, &hasWritten, NULL);
}

void board::redraw() {
	COORD coord;
	HANDLE hout = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD hasWritten;

	SetConsoleTextAttribute(hout, CONSOLE_DEFAULT);

	for (int i = 0; i < SZ; i++) {
		for (int j = 0; j < SZ; j++) {
			coord.Y = (SZ - i) * 2 - 1;
			coord.X = (j + 1) * 4 - 2;
			SetConsoleCursorPosition(hout, coord);
			WriteConsole(hout, L" ", 1, &hasWritten, NULL);
		}
	}
}


/* destructor */
board::~board() {
}
