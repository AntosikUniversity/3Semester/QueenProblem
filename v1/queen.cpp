#include "stdafx.h"

#ifndef __QUEEN__
#include "queen.h"
#endif // __QUEEN__

/* get & set*/
int queen::get_x() {
	return _x;
}

int queen::get_y() {
	return _y;
}

int queen::get_color() {
	return _color;
}

void queen::set_x(int x) {
	_x = x;
}

void queen::set_y(int y) {
	_y = y;
}

void queen::set_color(int color) {
	_color = color;
}



/* manilupaltion */
std::ostream & operator<<(std::ostream & stream, queen & q) {
	stream << (char)(q.get_x() + 65) << q.get_y()+1;
	return stream;
}

void queen::draw() {
}

void queen::set_pos(int x, int y) {
	_x = x;
	_y = y;
}

/* desctructor */
queen::~queen() {
}